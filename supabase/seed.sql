
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS "pgsodium" WITH SCHEMA "pgsodium";

CREATE EXTENSION IF NOT EXISTS "pg_graphql" WITH SCHEMA "graphql";

CREATE EXTENSION IF NOT EXISTS "pg_stat_statements" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "pgcrypto" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "pgjwt" WITH SCHEMA "extensions";

CREATE EXTENSION IF NOT EXISTS "supabase_vault" WITH SCHEMA "vault";

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA "extensions";

SET default_tablespace = '';

SET default_table_access_method = "heap";

CREATE TABLE IF NOT EXISTS "public"."transfer" (
    "id" bigint NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "email" character varying NOT NULL,
    "monto" numeric,
    "user_id" "uuid" DEFAULT "auth"."uid"() NOT NULL,
    "receiver_user_id" "text" DEFAULT ''::"text"
);

ALTER TABLE "public"."transfer" OWNER TO "postgres";

CREATE TABLE IF NOT EXISTS "public"."comisiones" (
    "id" bigint NOT NULL,
    "created_at" timestamp with time zone DEFAULT "now"() NOT NULL,
    "email" character varying NOT NULL,
    "monto" numeric,
    "user_id" "uuid" DEFAULT "auth"."uid"() NOT NULL,
    "receiver_user_id" "text" DEFAULT ''::"text"
);

ALTER TABLE "public"."transfer" OWNER TO "postgres";

ALTER TABLE "public"."transfer" ALTER COLUMN "id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME "public"."transfer_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);

ALTER TABLE "public"."comisiones" ALTER COLUMN "id" ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME "public"."comisiones_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);

CREATE TABLE IF NOT EXISTS "public"."usuarios" (
    "id" "uuid" NOT NULL,
    "email" "text"
);

ALTER TABLE "public"."usuarios" OWNER TO "postgres";

CREATE TABLE IF NOT EXISTS "public"."wallet" (
    "id" integer NOT NULL,
    "balance" numeric(10,2)
);

ALTER TABLE "public"."wallet" OWNER TO "postgres";

CREATE SEQUENCE IF NOT EXISTS "public"."wallet_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "public"."wallet_id_seq" OWNER TO "postgres";

ALTER SEQUENCE "public"."wallet_id_seq" OWNED BY "public"."wallet"."id";

ALTER TABLE ONLY "public"."wallet" ALTER COLUMN "id" SET DEFAULT "nextval"('"public"."wallet_id_seq"'::"regclass");

ALTER TABLE ONLY "public"."usuarios"
    ADD CONSTRAINT "profiles_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."transfer"
    ADD CONSTRAINT "transfer_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."comisiones"
    ADD CONSTRAINT "comisiones_pkey" PRIMARY KEY ("id");    

ALTER TABLE ONLY "public"."wallet"
    ADD CONSTRAINT "wallet_pkey" PRIMARY KEY ("id");

ALTER TABLE ONLY "public"."transfer"
    ADD CONSTRAINT "transfer_userId_fkey" FOREIGN KEY ("user_id") REFERENCES "auth"."users"("id");

ALTER TABLE ONLY "public"."comisiones"
    ADD CONSTRAINT "comisiones_userId_fkey" FOREIGN KEY ("user_id") REFERENCES "auth"."users"("id");    

ALTER TABLE ONLY "public"."usuarios"
    ADD CONSTRAINT "usuarios_userId_fkey" FOREIGN KEY ("id") REFERENCES "auth"."users"("id") ON DELETE CASCADE;

CREATE POLICY "Enable insert for authenticated users only" ON "public"."transfer" FOR INSERT TO "authenticated" WITH CHECK (true);

CREATE POLICY "Enable read access for all users" ON "public"."transfer" FOR SELECT USING (true);

CREATE POLICY "Enable insert for authenticated users only" ON "public"."comisiones" FOR INSERT TO "authenticated" WITH CHECK (true);

CREATE POLICY "Enable read access for all users" ON "public"."comisiones" FOR SELECT USING (true);


CREATE POLICY "Enable read access for all users" ON "public"."usuarios" FOR SELECT USING (true);

ALTER TABLE "public"."transfer" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."comisiones" ENABLE ROW LEVEL SECURITY;

ALTER TABLE "public"."usuarios" ENABLE ROW LEVEL SECURITY;

CREATE FUNCTION "public"."handle_new_user"() RETURNS "trigger"
    LANGUAGE "plpgsql" SECURITY DEFINER
    AS $$
begin
  insert into public.usuarios (id, email)
  values (new.id, new.email);
  return new;
end;
$$;
create trigger on_auth_user_created
  after insert on auth.users
  for each row execute procedure public.handle_new_user();




GRANT ALL ON FUNCTION "public"."handle_new_user"() TO "anon";
GRANT ALL ON FUNCTION "public"."handle_new_user"() TO "authenticated";
GRANT ALL ON FUNCTION "public"."handle_new_user"() TO "service_role";




GRANT USAGE ON SCHEMA "public" TO "postgres";
GRANT USAGE ON SCHEMA "public" TO "anon";
GRANT USAGE ON SCHEMA "public" TO "authenticated";
GRANT USAGE ON SCHEMA "public" TO "service_role";

GRANT ALL ON TABLE "public"."transfer" TO "anon";
GRANT ALL ON TABLE "public"."transfer" TO "authenticated";
GRANT ALL ON TABLE "public"."transfer" TO "service_role";

GRANT ALL ON SEQUENCE "public"."transfer_id_seq" TO "anon";
GRANT ALL ON SEQUENCE "public"."transfer_id_seq" TO "authenticated";
GRANT ALL ON SEQUENCE "public"."transfer_id_seq" TO "service_role";

GRANT ALL ON TABLE "public"."usuarios" TO "anon";
GRANT ALL ON TABLE "public"."usuarios" TO "authenticated";
GRANT ALL ON TABLE "public"."usuarios" TO "service_role";

GRANT ALL ON TABLE "public"."wallet" TO "anon";
GRANT ALL ON TABLE "public"."wallet" TO "authenticated";
GRANT ALL ON TABLE "public"."wallet" TO "service_role";

GRANT ALL ON SEQUENCE "public"."wallet_id_seq" TO "anon";
GRANT ALL ON SEQUENCE "public"."wallet_id_seq" TO "authenticated";
GRANT ALL ON SEQUENCE "public"."wallet_id_seq" TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON SEQUENCES  TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON FUNCTIONS  TO "service_role";

ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "postgres";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "anon";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "authenticated";
ALTER DEFAULT PRIVILEGES FOR ROLE "postgres" IN SCHEMA "public" GRANT ALL ON TABLES  TO "service_role";

RESET ALL;
