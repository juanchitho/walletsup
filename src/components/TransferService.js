//import { createContext , useContext , useState } from "react";
import supabase from "../supabase/supabase";


// Función para obtener todas las transferencias recibidas por un usuario
async function getReceivedTransfers(userId) {
    try {
        const { data, error } = await supabase
        .from('transfer')
        .select('*')
        .eq('receiver_user_id', userId);
        console.log("userId del transferservice",userId);
        
    if (error) {
      throw error;
    }
    console.log("--------",data);
    return data;
  } catch (error) {
    console.error('Error al obtener las transferencias recibidas:', error);
    return [];
  }
}

// Función para obtener todas las transferencias enviadas por un usuario
async function getSentTransfers(userId) {
  try {
    const { data, error } = await supabase
      .from('transfer')
      .select('*')
      .eq('user_id', userId);

      

    if (error) {
      throw error;
    }
    console.log("--------",data);
    return data;
  } catch (error) {
    console.error('Error al obtener las transferencias enviadas:', error);
    return [];
  }
}
async function getReceivedComisiones(userId) {
  try {
      const { data, error } = await supabase
      .from('comisiones')
      .select('*')
      .eq('receiver_user_id', userId);
      console.log("userId del transferservice",userId);
      
  if (error) {
    throw error;
  }
  console.log("--------",data);
  return data;
} catch (error) {
  console.error('Error al obtener las transferencias recibidas:', error);
  return [];
}
}

// Función para obtener todas las transferencias enviadas por un usuario
async function getSentComisiones(userId) {
try {
  const { data, error } = await supabase
    .from('comisiones')
    .select('*')
    .eq('user_id', userId);

    

  if (error) {
    throw error;
  }
  console.log("--------",data);
  return data;
} catch (error) {
  console.error('Error al obtener las transferencias enviadas:', error);
  return [];
}
}

export { getReceivedTransfers, getSentTransfers, getSentComisiones, getReceivedComisiones };
