/* import logo from './logo.svg'; */
import './App.css';
import { useEffect } from 'react';
import Login from './pages/Login';
import SignUp from './pages/SignUp';
import Wallet from './pages/Wallet';
import NotFound from './pages/NotFound';
import supabase from './supabase/supabase';
import { Routes , Route , useNavigate} from 'react-router-dom';
import { TransferContextProvider } from './context/TransferContex';

function App() {

  const navigate = useNavigate();

  useEffect(() => {
    supabase.auth.onAuthStateChange((event, session) => {
    if (!session) {
      navigate('/login');
    }else{
      navigate('/walletsup');
    }
  })
    

    }, []);
  return (
    <div className="App">
      <TransferContextProvider>
        <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/signup" element={<SignUp />} />
            <Route path="/walletsup" element={<Wallet />} />
            <Route path="*" element={<NotFound />} />

          </Routes>
      </TransferContextProvider>
    

    </div>
  );
}

export default App;
