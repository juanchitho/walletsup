import React, { useState, useEffect } from "react";
import supabase from "../supabase/supabase";
import { useNavigate } from "react-router-dom";
import Transfer from "../components/Transfer";
import TransferList from "../components/TransferList";
import { getReceivedTransfers, getSentTransfers, getReceivedComisiones, getSentComisiones } from "../components/TransferService";

import { useTransfer } from "../context/TransferContex";

const Wallet = () => {
  const [balance, setBalance] = useState(0);
  const navigate = useNavigate();
  const { historial, setName } = useTransfer();
  const [email,setEmail] = useState();
  const [receivedTransfers, setReceivedTransfers] = useState([]);
  const [sentTransfers, setSentTransfers] = useState([]);
  const [receivedComisiones, setReceivedComisiones] = useState([]);
  const [sentComisiones, setSentComisiones] = useState([]);
  console.log("este es el historial", historial); //me puede ser util para traerme el monto

  useEffect(() => {
    if (!supabase.auth.getUser()) {
      navigate("/login");
    }
  }, [navigate]);

    // Obtener las transferencias recibidas y enviadas del usuario actual
    useEffect(() => {
      const fetchTransfers = async () => {
        const user = await supabase.auth.getUser();
        if (user) {
          const user_id = user.data.user.id;
          const email = user.data.user.email
          //console.log("testeo mail",email)
          const received = await getReceivedTransfers(user_id);
          const sent = await getSentTransfers(user_id);
          const receivedComisiones = await getReceivedComisiones(user_id);
          const sentComisiones = await getSentComisiones(user_id);
          console.log("testeo received",received ,sent)
          setReceivedTransfers(received);
          setSentTransfers(sent);
          setReceivedComisiones(receivedComisiones);
          setSentComisiones(sentComisiones);
          console.log("testeo comisionessssss",receivedComisiones ,sentComisiones)
          setEmail(email);
        }
      };
  
      fetchTransfers();
    }, []);

    // Calcular el saldo en respuesta a cambios en las transferencias recibidas y enviadas
    useEffect(() => {
      const receivedTotal = receivedTransfers.reduce((total, transfer) => total + transfer.monto, 0);
      const sentTotal = sentTransfers.reduce((total, transfer) => total + transfer.monto, 0);
      const receivedTotalComisiones = receivedComisiones.reduce((total, transfer) => total + transfer.monto, 0);
      const sentTotalComisiones = sentComisiones.reduce((total, transfer) => total + transfer.monto, 0);
      console.log("testeo receivedTotal",receivedTotal ,sentTotal)
      const calculatedBalance = receivedTotal - sentTotal + receivedTotalComisiones - sentTotalComisiones;
      console.log("testeo milllll",receivedTotalComisiones,sentTotalComisiones)
      setBalance(calculatedBalance);
    }, [receivedTransfers, sentTransfers, receivedComisiones, sentComisiones]);
    
    
    

  const addFunds = async (amount) => {
    // Obtener el ID de la billetera que deseas actualizar (puedes cambiar esto según tu lógica)
    const walletId = 1; // Cambia esto para que coincida con la billetera que deseas actualizar

    // Actualizar el saldo de la billetera en Supabase utilizando una cláusula WHERE
    const { error } = await supabase
      .from("wallet")
      .update({ balance: balance + amount })
      .eq("id", walletId); // Establece la condición WHERE

    if (error) {
      console.error("Error al agregar fondos:", error.message);
    } else {
      setBalance(balance + amount);
    }
  };

  return (
    <div>
      <h1>Billetera de {email}</h1>
      <button
        onClick={() => {
          supabase.auth.signOut();
          setName([]);
        }}
      >
        {" "}
        LogOut{" "}
      </button>
      <p>Saldo: ${balance}</p>
      <button onClick={() => addFunds(10)}>Agregar $10</button>
      <Transfer />
      <TransferList />
    </div>
  );
};

export default Wallet;
