import { useState, useEffect } from "react";
import supabase from "../supabase/supabase";
import { useNavigate } from "react-router-dom";
import { useTransfer } from "../context/TransferContex";

function SignUp() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const { setHistorialDesactualizado } = useTransfer();

  useEffect(() => {
    if (!supabase.auth.getUser()) {
      navigate("/login");
    }
  }, [navigate]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
        const { data, error } =  await supabase.auth.signUp({
        email,
        password,
      });
      setHistorialDesactualizado(true);
      console.log(data ,error);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1> SignUp </h1>
      <form onSubmit={handleSubmit}>
        <input
          type="email"
          name="email"
          placeholder="youremail@site.com"
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="password"
          name="password"
          placeholder="password"
          onChange={(e) => setPassword(e.target.value)}
        />

        <button> SignUp </button>

      </form>
    </div>
  );
}

export default SignUp;
